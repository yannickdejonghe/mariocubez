var numHeroes = 0;


var Hero = function(n, p, h, w, G) {
	numHeroes++;
	console.log(numHeroes + " heroes in the game");
	var name = n;
	var position = p;
	var height = h;
	var width = w;
	var color = "red";
	var jumpSpeed = 0;
	var jumpCount = 0;
	var maxJumpSpeed =320;
	var forwardSpeed = 0;
	var gravity = G;
	var canvas = $("canvas");
	var drawingContext = $("canvas").getContext('2d');

	var jumpSound = new Audio('sound/jump.mp3');
	jumpSound.volume = 0.1;



	return {
		Type: "hero",
		HasGravity: true,
		CanCollide: true,
		CanUpdatePosition: true,
		Drawable: true,
		Destroyed: false,
		BottomLeft: function() {return this.GetPosition()},
		BottomRight: function() {return new Point(this.GetPosition().X + width, this.GetPosition().Y)},
		TopLeft: function(){return new Point(this.GetPosition().X, this.GetPosition().Y - height)},
		TopRight: function() {return  new Point(this.GetPosition().X + width, this.GetPosition().Y - height)},
		Destroy: function() {
			this.Destroyed = true;
		},
		OnCollision: function(collision) {
			if(collision.Other.Type == 'coin') {
					collision.Other.Destroy();
					var audio = new Audio('http://themushroomkingdom.net/sounds/wav/smw/smw_coin.wav');
					audio.play();
					return;
				}
			console.log(collision)

			if(collision.Side == CollisionSides.Bottom && collision.Other.CanCollide) {

				if(collision.Other.Type == "enemy" || collision.Other.Type == "coin") {
									console.log('top hero');

					collision.Other.Destroy();
					return;
				}


				else if(jumpSpeed < 0) {
					this.ResetJumpSpeed();
					position = new Point(position.X, collision.Other.BottomLeft().Y + (collision.Other.TopLeft().Y - collision.Other.BottomLeft().Y));
			
				}
				else {
					jumpSpeed = 0;
					position = new Point(position.X, collision.Other.BottomLeft().Y + height);
				}
			}

			else if (collision.Side == CollisionSides.Top && collision.Other.CanCollide) {
				jumpSpeed = 0;
					position = new Point(position.X, collision.Other.BottomLeft().Y + height);
			} 

			else if(collision.Side == CollisionSides.Right) {
				

				if(collision.Other.Type == "enemy") {
					this.Destroy();
				}
				else {
	forwardSpeed = 0;
						position = new Point(collision.Other.BottomLeft().X - width, position.Y);
						
				}
			}

			else if(collision.Side == CollisionSides.Left) {
				forwardSpeed = 0;
						position = new Point(collision.Other.BottomRight().X , position.Y);

					
			}
		},
		Name: this.name,
		Jump: function() {
			if(jumpCount < 2) {
				jumpSound.currentTime = 0;
				jumpSound.play();
				jumpSpeed = maxJumpSpeed;
				jumpCount++;
			}
		},
		GetJumpSpeed: function() {
			return jumpSpeed;
		},	
		ResetJumpSpeed: function() {
			jumpSpeed = 0;
			jumpCount = 0;
		},
		GetBase: function() {
			return {
				Point1: position,
				Point2: new Point(position.X + width, position.Y)
			}
		},
		GetPosition: function() {
				return position;
		},
		SetPosition: function(p) {
				position = p;
		},
		isJumping: false, 
		Shoot: function() {

		},
		ChangeSpeed: function(speed) {
			forwardSpeed = speed;

		},
		MoveX: function(x) {
			position.X += x ;
		},

		GetSpeed: function() {
			return forwardSpeed * Helpers.deltaT / 1000;
		},
		
		Draw: function() {
			drawingContext.fillStyle = color;
			drawingContext.fillRect(position.X, position.Y, width, -height);
		},

		UpdatePosition: function() {
			jumpSpeed = jumpSpeed - gravity * Helpers.deltaT / 1000;
			position.Y -= jumpSpeed * Helpers.deltaT /1000;
			if(Helpers.PlayerIsInViewPort(this.GetBase().Point1, this.GetBase().Point2)) {
				position.X += forwardSpeed * Helpers.deltaT / 1000;
			}
			
		}
	}
}