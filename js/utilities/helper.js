

var Helpers = {
	PlayerIsInViewPort: function(point1, point2) {
		var canvas = $('canvas');

		if(point1.X > canvas.width/5 && point1.X < canvas.width/5 * 4 && point2.X > canvas.width/5 && point2.X < canvas.width/5 * 4) {
			return true;
		}

		return false;
	},

	deltaT: 0,

	Avg: function(val1, val2) {
		return (val1 + val2) / 2;
	}
}