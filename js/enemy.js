



var Enemy = function(p, h, w, G) {
	var position = p;
	var height = h;
	var width = w;
	var color = "yellow";
	var jumpSpeed = 0;
	var jumpCount = 0;
	var maxJumpSpeed = 5;
	var forwardSpeed = 50;
	var gravity = G;
	var canvas = $("canvas");
	var drawingContext = $("canvas").getContext('2d');
	var totaltime = 0;
	var startx = 0;



	return {
		Type: "enemy",
		HasGravity: true,
		CanUpdatePosition: true,
		CanCollide: true,
		Drawable: true,
		Destroyed: false,
		BottomLeft: function() {return this.GetPosition()},
		BottomRight: function() {return new Point(this.GetPosition().X + width, this.GetPosition().Y)},
		TopLeft: function(){ return new Point(this.GetPosition().X, this.GetPosition().Y - height)},
		TopRight: function() {return  new Point(this.GetPosition().X + width, this.GetPosition().Y - height)},	
		Destroy: function() {
			console.log('enemy gets destroyed');
			this.Destroyed = true;
			
		},
		OnCollision: function(collision) {
			if(collision.Other.Type != 'coin') {
				if(collision.Side == CollisionSides.Bottom && collision.Other.CanCollide) {
					if(jumpSpeed < 0) {
						position = new Point(position.X, collision.Other.BottomLeft().Y + (collision.Other.TopLeft().Y - collision.Other.BottomLeft().Y));
					
						
					}
					else {
						position = new Point(position.X, collision.Other.BottomLeft().Y + height);
					}
					this.ResetJumpSpeed();
				}

				else if((collision.Side == CollisionSides.Right || collision.Side == CollisionSides.Left)  && collision.Other.CanCollide) {
					
					var b = Math.abs(Helpers.Avg(position.X, position.X + width) - collision.Other.BottomLeft().X) >= Math.abs(Helpers.Avg(position.X, position.X + width) - collision.Other.BottomRight().X);
					

					

					switch(b) { 
						case false:
							console.log('jaja');
							position = new Point(collision.Other.BottomLeft().X - width, position.Y);
							break;
						default:
							position = new Point(collision.Other.BottomRight().X , position.Y);
							break;

					}

					if(collision.Other.Type == "obstacle") {
						forwardSpeed = -forwardSpeed;
					}
				}
			}
		},
		GetBase: function() {
			return {
				Point1: new Point(position.X, position.Y),
				Point2: new Point(position.X + width, position.Y)
			}
		},
		Time: totaltime,
		GetPosition: function() {
			return position;
		},
		SetPosition: function(p) {
			position = p;
		},
		GetJumpSpeed: function() {
			return jumpSpeed;
		},
		ResetJumpSpeed: function() {
			jumpSpeed = 0;
		},
		ChangeSpeed: function(speed) {
			forwardSpeed = speed;

		},

		GetSpeed: function() {
			return forwardSpeed;
		},
		
		Draw: function() {
			var img = $('enemyImage');
			drawingContext.drawImage(img, position.X, position.Y, width, -height);
		},

		Die: function() {
			//jumpSpeed = 5;
		},

		MoveX: function(x) {
			position.X += x;
		},

		UpdatePosition: function() {
			if(totaltime = 0) {
				startx = position.X;
			}
			totaltime += Helpers.deltaT / 1000;

			jumpSpeed = jumpSpeed - gravity * Helpers.deltaT / 1000;
			position.Y -= jumpSpeed * Helpers.deltaT / 1000;
			position.X += forwardSpeed * Helpers.deltaT / 1000
		
		}
	}
}