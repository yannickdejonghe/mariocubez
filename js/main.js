(function() {


	var canvas = $("canvas");

	var world = new World();

	
	var initPlatforms = function() {
		var p = []
		// Ground
		p.push(new Obstacle(new Point(-1000,canvas.height), 3090, 20));
		// Platforms
		 p.push(new Obstacle(new Point(canvas.width/2,canvas.height - 80), 60, 20));
		 p.push(new Obstacle(new Point(250,canvas.height - 20), 10, 20));
		 p.push(new Obstacle(new Point(100,canvas.height - 20), 10, 20));


		 p.push(new Obstacle(new Point(canvas.width/4 * 1.5,canvas.height - 90), 60, 20));
		 p.push(new Obstacle(new Point(canvas.width/2 * 2,canvas.height - 80), 100, 20));

		 p.push(new Obstacle(new Point(canvas.width/2 * 1.5,canvas.height - 140), 60, 20));


		return p;
	}

	var initCoins = function() {
		var c = [];


		for(var i = 0; i < 1000; i+= 50) {
			for(var j = 0; j < 1000; j+= 50) {
				c.push(new Coin(new Point(i,j), 10,10));
			}
		}

		return c;
	}

	var initEnemies = function() {
		var e = [];
		e.push(new Enemy(new Point(canvas.width/2,canvas.height - 200), 15, 15, world.Gravity));

		return e;
	}

	var init = function() {
		world = new World();

		var player = new Hero("mario", new Point(canvas.width / 5 + 10,canvas.height - 80), 30, 25, world.Gravity);
		var platforms = initPlatforms();
		var enemies = initEnemies();

		world.Player = player;

		world.GameObjects = world.GameObjects.concat(platforms);
				world.GameObjects.push(player);

		world.GameObjects = world.GameObjects.concat(enemies);

		world.GameObjects = world.GameObjects.concat(initCoins());

		console.log(world.GameObjects);
		//start gameloop
		//window.setInterval(gl, 3);
		window.requestAnimationFrame(gl);
	}

	var handleCollisions = function(obj1, obj2) {

		width1 = Math.abs(obj1.TopLeft().X - obj1.TopRight().X);
		height1 = Math.abs(obj1.TopLeft().Y - obj1.BottomLeft().Y);

		width2 = Math.abs(obj2.TopLeft().X - obj2.TopRight().X);
		height2 = Math.abs(obj2.TopLeft().Y - obj2.BottomLeft().Y);

		var w = (width1 + width2) / 2;
		var h = (height1 + height2) / 2;

		// calc centers
		var dx = (obj1.TopLeft().X + (width1/2)) - (obj2.TopLeft().X + (width2/2));
		var dy = (obj1.TopLeft().Y + (height1/2)) - (obj2.TopLeft().Y + (height2/2));


		if(Math.abs(dx) <= w && Math.abs(dy) <= h) {
			var wy = w*dy;
			var hx = h * dx;

			if(wy > hx) {
				if(wy > -hx) {
					obj1.OnCollision(new Collision(CollisionSides.Top, obj2));
					obj2.OnCollision(new Collision(CollisionSides.Bottom, obj1)); // Top
				}
				else {
					obj1.OnCollision(new Collision(CollisionSides.Right, obj2));
					obj2.OnCollision(new Collision(CollisionSides.Left, obj1)); // Left
				}
			}
			else {
				if(wy > -hx) {
					obj1.OnCollision(new Collision(CollisionSides.Left, obj2));
					obj2.OnCollision(new Collision(CollisionSides.Right, obj1)); // Right
				}
				else {
					obj1.OnCollision(new Collision(CollisionSides.Bottom, obj2));
					obj2.OnCollision(new Collision(CollisionSides.Top, obj1)); // Bottom
				}
			}

		}


		// if (obj1.BottomRight().X > obj2.BottomLeft().X && obj1.BottomLeft().X  < obj2.BottomRight().X) {
		// 	if (obj1.BottomRight().Y > obj2.TopRight().Y && obj1.TopRight().Y  < obj2.BottomRight().Y) {
				



				// var offsetX = Math.max(0, Math.min(obj1.TopRight().X, obj2.TopRight().X) - Math.max(obj1.TopLeft().X, obj2.TopLeft().X));
				// var offsetY = Math.max(0, Math.min(obj1.BottomRight().Y, obj2.BottomRight().Y) - Math.max(obj1.TopLeft().Y, obj2.TopLeft().Y));
				
				
				// var isHorizontal = offsetX < offsetY ? true : false;

				// if(isHorizontal) {
					

				// 	obj1.OnCollision(new Collision(CollisionSides.Right, obj2));
				// 	obj2.OnCollision(new Collision(CollisionSides.Right, obj1));
				// }
				// else {
				// 	var collisionSide1 = (offsetY >= 0) ? CollisionSides.Top : CollisionSides.Bottom;
				// 	var collisionSide2 = (offsetY >= 0) ? CollisionSides.Bottom : CollisionSides.Top;

				// 	obj1.OnCollision(new Collision(CollisionSides.Bottom, obj2));
				// 	obj2.OnCollision(new Collision(CollisionSides.Bottom	, obj1));
				// }
		// 	}	
		// }


	}

	var previous = null;
	var gl = function(timestamp) {

		if(previous != null) {
			Helpers.deltaT = timestamp - previous;
		}
		
		previous = timestamp;

		// Update positions
		for(var i = 0; i < world.GameObjects.length; i++) {
			var object = world.GameObjects[i];
			if(object != undefined) {
				if(object.CanUpdatePosition) {
					object.UpdatePosition();
					
				}
			}
		}


		if(world.Player.GetSpeed()!= 0) {
			if(!Helpers.PlayerIsInViewPort(world.Player.GetBase().Point1, world.Player.GetBase().Point2)) {
			
				for(var i = 0; i < world.GameObjects.length; i++) {
					if(world.GameObjects[i].Type != "hero")
						world.GameObjects[i].MoveX(-world.Player.GetSpeed());
				}

				

				world.Player.SetPosition(new Point(world.Player.GetPosition().X - world.Player.GetSpeed(), world.Player.GetPosition().Y));
			}

		}		


		// collistiondetection
		for(var i = 0; i < world.GameObjects.length; i++) {
			var object = world.GameObjects[i];
			if(object.CanCollide) {
				for(var j = i + 1; j < world.GameObjects.length; j++) {
					if((object.CanUpdatePosition || world.GameObjects[j].CanUpdatePosition) && world.GameObjects[j].CanCollide) {
						handleCollisions(object, world.GameObjects[j]);
					}
				}
			}
		}

		canvas.getContext('2d').clearRect(0,0,canvas.width,canvas.height);
		// draw objects
		for(var i = 0; i < world.GameObjects.length; i++) {
			var object = world.GameObjects[i];

			if(world.GameObjects[i].Destroyed) {
				world.GameObjects.splice(i,1);
			}

			else if(object.Drawable) {
				object.Draw();
			}
		}




		window.requestAnimationFrame(gl);
	}

		init();






















	// var creacteObjects = function() {
	// 	var audio = new Audio('http://216.227.134.162/ost/super-mario-bros/gipwwbutdn/01-super-mario-bros.mp3');
	// 	audio.play();

	// 	// Ground
	// 	world.Obstacles.push(new Obstacle(new Point(-1000,canvas.height - 10), 3090, 20));

	// 	// Platforms
	// 	world.Obstacles.push(new Obstacle(new Point(canvas.width/2,canvas.height - 60), 40, 20));
	// 	world.Obstacles.push(new Obstacle(new Point(canvas.width/2 * 1.5,canvas.height - 80), 40, 20));
	// 	world.Obstacles.push(new Obstacle(new Point(canvas.width/2 * 2,canvas.height - 60), 100, 20));
	// 	world.Obstacles.push(new Obstacle(new Point(canvas.width/2 * 1.5,canvas.height - 80), 40, 20));

	// 	// Enemies
	// 	world.Enemies.push(new Enemy(new Point(canvas.width/2,canvas.height - 100), 10, 10, world.Gravity));
	// 	world.Enemies.push(new Enemy(new Point(canvas.width/2*3,canvas.height - 20), 10, 10, world.Gravity));
	// }
	
	//creacteObjects();

	// var gameLoop = function() {
	// 	canvas.getContext('2d').clearRect(0,0,canvas.width,canvas.height);


	// 	//update
	// 	player.UpdatePosition();
	// 	for(var i = 0; i <  world.Obstacles.length; i++) {
	// 		if(world.Obstacles[i].UpdatePosition) {
	// 			world.Obstacles[i].UpdatePosition();
	// 		}
	// 	}

	// 	for(var i = 0; i < world.Enemies.length; i++) {
	// 		if(world.Enemies[i].UpdatePosition) {
	// 			world.Enemies[i].UpdatePosition();
	// 		}
	// 	}


	// 	if(player.GetSpeed()!= 0) {
	// 		if(!Helpers.PlayerIsInViewPort(player.GetBase().Point1, player.GetBase().Point2)) {
			
	// 			for(var i = 0; i < world.Obstacles.length; i++) {
	// 				world.Obstacles[i].MoveX(-player.GetSpeed());
	// 			}

	// 			for(var i = 0; i < world.Enemies.length; i++) {
	// 				world.Enemies[i].MoveX(-player.GetSpeed());
	// 			}

	// 			player.SetPosition(new Point(player.GetPosition().X - player.GetSpeed(), player.GetPosition().Y));
	// 		}

	// 	}		
	// 	// collistiondetection
	// 	for(var i = 0; i <  world.Obstacles.length; i++) {
	// 			if(objectsAreColliding(player, world.Obstacles[i]) && player.GetJumpSpeed() < 0) {
	// 				player.ResetJumpSpeed();
	// 				player.SetPosition(new Point(player.GetPosition().X, world.Obstacles[i].GetBase().Point1.Y));
					
	// 			}

	// 			for(var j = 0; j<  world.Enemies.length; j++) {
	// 				if(objectsAreColliding(world.Enemies[j], world.Obstacles[i])) {
	// 					world.Enemies[j].ResetJumpSpeed();
	// 					world.Enemies[j].SetPosition(new Point(world.Enemies[j].GetPosition().X, world.Obstacles[i].GetBase().Point1.Y));
						
	// 				}
	// 			}
		
	// 	}

	// 	for(var i = 0; i <  world.Enemies.length; i++) {
	// 			if(objectsAreColliding(player, world.Enemies[i]) && player.GetJumpSpeed() < 0) {
	// 				player.ResetJumpSpeed();
	// 				player.Jump();
	// 				player.SetPosition(new Point(player.GetPosition().X, world.Enemies[i].GetBase().Point1.Y));
					
	// 			}
	// 	}

	// 	// draw
	// 	for(var i = 0; i <  world.Obstacles.length; i++) {
	// 		world.Obstacles[i].Draw();
	// 	}
	// 	for(var i = 0; i <  world.Enemies.length; i++) {
	// 		world.Enemies[i].Draw();
	// 	}


	// 	player.Draw();


	// 	window.requestAnimationFrame(gameLoop);

	// }


	


	// Event listeners
	document.addEventListener('keydown', function(e) {
		if(e.keyCode == 32) {
				e.preventDefault();

				world.Player.Jump();
		}
		else if(e.keyCode == 39) {
			e.preventDefault();

			// moving forward
			
				world.Player.ChangeSpeed(90);
		}
		else if(e.keyCode == 37) {
			e.preventDefault();

			
				world.Player.ChangeSpeed(-90);
		}
	});
	document.addEventListener('keyup', function(e) {
		e.preventDefault();
		if(e.keyCode == 39 || e.keyCode == 37) {
				
				world.Player.ChangeSpeed(0);
		}
	});
	
		



	
	

	// looooop




})();
