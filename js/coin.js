var Coin =  function(p,h,w) {
	var position = p;
	var height = h;
	var width = w;
	var color = 'orange';
	var canvas = $("canvas");
	var drawingContext = $("canvas").getContext('2d');
	return {
		Type: 'coin',
		CanCollide: true,
		BottomLeft: function() {return this.GetPosition()},
		BottomRight: function() {return new Point(this.GetPosition().X + width, this.GetPosition().Y)},
		TopLeft: function(){return new Point(this.GetPosition().X, this.GetPosition().Y - height)},
		TopRight: function() {return  new Point(this.GetPosition().X + width, this.GetPosition().Y - height)},
		MoveX: function(x) {
			position.X += x;
		},
		GetPosition: function() {
				return position;
		},
		OnCollision: function() {

			console.log('jow');
		},
		Draw: function() {
			var image = $('coinImage');
			drawingContext.drawImage(image,	position.X, position.Y, width, -height);
		},
		UpdatePosition: function() {
			
		
		},
		CanUpdatePosition: false,
		Destroyed: false,
		Destroy: function() {
			this.Destroyed = true;
		},
		Drawable: true


	}
}