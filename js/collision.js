var CollisionSides = {
	Left: "left",
	Right: "right",
	Top: "top",
	Bottom: "bottom"
};


var Collision = function(side, otherObject) {
	return {
		Side: side,
		Other: otherObject
	}
}