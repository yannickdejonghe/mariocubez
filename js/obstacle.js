var Obstacle = function(p, w, h) {
	var position = p;
	var height = h;
	var width = w;
	var color = "blue";
	var canvas = $("canvas");
	var drawingContext = $("canvas").getContext('2d');


	return {
		Type: 'obstacle',
		HasGravity: false,
		CanUpdatePosition: false,
		CanCollide: true,
		Drawable: true,
		Destroyed: false,
		BottomLeft: function() {return this.GetPosition()},
		BottomRight: function() {return new Point(this.GetPosition().X + width, this.GetPosition().Y)},
		TopLeft: function(){ return new Point(this.GetPosition().X, this.GetPosition().Y - height)},
		TopRight: function() {return  new Point(this.GetPosition().X + width, this.GetPosition().Y - height)},
		Destroy: function() {
			this.Destroyed = true;
		},
		OnCollision: function(other) {

		},
		GetPosition: function() {
				return position;
		},
		Draw: function() {
			var img = $('brickImage');
			var pat = drawingContext.createPattern(img,"repeat-x");

			drawingContext.drawImage(img, 
        position.X, 
        position.Y,
        width, -height);

			// drawingContext.translate(position.X, position.Y );

			// drawingContext.fillStyle = pat;

			// drawingContext.fillRect(0, 0, width, -height);

			// drawingContext.translate(-position.X, -position.Y);
		},
		
		GetBase: function() {
			return {
				Point1: position,
				Point2: new Point(position.X + width, position.Y)
			}
		},

		GetJumpSpeed: function() {
			return 0;
		},

		MoveX: function(x) {
			position.X += x;
		}
	}
}